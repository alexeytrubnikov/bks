package com.bks.controller;

import com.bks.model.inputEntity.Portfolio;
import com.bks.model.inputEntity.Stock;
import com.bks.model.outputEntity.CalculatePortfolio;
import com.bks.service.PortfolioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/")
public class PortfolioController {

    @Autowired
    private PortfolioService portfolioService;


    @RequestMapping(value = "/calculate", method = RequestMethod.POST)
    @ResponseBody
    public CalculatePortfolio inputData(@RequestBody Portfolio portfolio){

        return portfolioService.calculate(portfolio);

    }

}