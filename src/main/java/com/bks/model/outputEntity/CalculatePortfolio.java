package com.bks.model.outputEntity;

import com.bks.model.base.Quote;
import com.bks.model.base.Sector;
import com.bks.model.inputEntity.Stock;
import com.bks.serialize.DoubleSerializer;
import com.bks.serialize.SectorSerializer;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.deser.std.NumberDeserializers;
import lombok.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Getter
@Setter
@NoArgsConstructor
public class CalculatePortfolio {

    private double value;
    private Set<Allocation> allocations = new HashSet<>();

    public void addAllocation(Quote quote, Stock stock){

        if (quote == null)
            return;

        if (getAllocations().stream().map(al -> al.getSector()).collect(Collectors.toList()).contains(quote.getCompany().getSector())){
            getAllocations().stream().filter(allocation -> allocation.getSector().equals(quote.getCompany().getSector())).forEach(allocation ->
                allocation.addAssetValue(quote.getLatestPrice() * stock.getVolume()));
        }else{
            getAllocations().add(new Allocation(quote.getCompany().getSector(), quote.getLatestPrice() * stock.getVolume()));
        }

        updateAllocationProportion();

    }

    private void updateAllocationProportion(){

        getAllocations().stream().forEach(allocation -> allocation.setProportion(allocation.getAssetValue() / getValue()));

    }


    private void addValue(double value){
        this.value += value;
    }


    @Getter
    @Setter
    @NoArgsConstructor
    @EqualsAndHashCode(of = "sector")
    private class Allocation{

        @JsonSerialize(using = SectorSerializer.class)
        private Sector sector;

        @JsonSerialize(using = DoubleSerializer.class)
        private double assetValue;
        @JsonSerialize(using = DoubleSerializer.class)
        private double proportion;

        public Allocation(Sector sector, double assetValue) {

            this.sector = sector;
            addAssetValue(assetValue);


        }
        public void addAssetValue(double assetValue){

            this.assetValue += assetValue;
            addValue(assetValue);

        }

    }


}