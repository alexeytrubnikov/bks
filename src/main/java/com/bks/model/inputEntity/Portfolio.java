package com.bks.model.inputEntity;

import com.bks.deserialize.PortflioDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@JsonDeserialize(using = PortflioDeserializer.class)
public class Portfolio {

    private Set<Stock> stocks = new HashSet<>();

}