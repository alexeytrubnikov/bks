package com.bks.model.base;

import com.bks.deserialize.CompanyDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "symbol")
@JsonDeserialize(using = CompanyDeserializer.class)
public class Company {

    private String symbol;
    private Sector sector;
    private String name;

}