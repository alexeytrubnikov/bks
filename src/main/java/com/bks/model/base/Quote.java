package com.bks.model.base;

import com.bks.deserialize.QuoteDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.omg.CosNaming.NamingContextExtPackage.StringNameHelper;

@Getter
@Setter
@NoArgsConstructor
@JsonDeserialize(using = QuoteDeserializer.class)
public class Quote {

    private Company company;
    private double latestPrice;


}