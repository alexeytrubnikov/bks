package com.bks.service;

import com.bks.model.inputEntity.Portfolio;
import com.bks.model.outputEntity.CalculatePortfolio;

public interface PortfolioService {

    CalculatePortfolio calculate(Portfolio portfolio);

}