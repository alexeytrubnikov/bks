package com.bks.service.impl;

import com.bks.model.base.Company;
import com.bks.model.base.Quote;
import com.bks.model.inputEntity.Portfolio;
import com.bks.model.inputEntity.Stock;
import com.bks.model.outputEntity.CalculatePortfolio;
import com.bks.service.PortfolioService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.annotation.Resource;
import java.net.URI;
import java.net.URISyntaxException;

@Service
public class PortfolioServiceImpl implements PortfolioService {

    @Resource
    private Environment env;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private ObjectMapper mapper;

    private Company getCompanyBySymbol(String symbol) throws URISyntaxException {

        Company company = new Company();

        StringBuilder builder = new StringBuilder();
        builder.append(env.getRequiredProperty("url.api"));
        builder.append(symbol);
        builder.append("/company");

        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUri(new URI(builder.toString()));
        uriBuilder.queryParam("token", env.getRequiredProperty("s_token"));

        ResponseEntity<String> httpResult = restTemplate.getForEntity(uriBuilder.toUriString(), String.class);

        if (httpResult.getStatusCode().is2xxSuccessful()){

            try {

                company = mapper.readValue(httpResult.getBody(), Company.class);

            }catch (JsonProcessingException e){

                e.printStackTrace();

            }

        }

        return company;


    }

    private Quote getQuoteBySymbol(String symbol) throws URISyntaxException{

        Quote quote = new Quote();

        StringBuilder builder = new StringBuilder();
        builder.append(env.getRequiredProperty("url.api"));
        builder.append(symbol);
        builder.append("/quote");

        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUri(new URI(builder.toString()));
        uriBuilder.queryParam("token", env.getRequiredProperty("s_token"));

        ResponseEntity<String> httpResult = restTemplate.getForEntity(uriBuilder.toUriString(), String.class);

        if (httpResult.getStatusCode().is2xxSuccessful()){

            try {

                quote = mapper.readValue(httpResult.getBody(), Quote.class);
                quote.setCompany(getCompanyBySymbol(symbol));

            }catch (JsonProcessingException e){

                e.printStackTrace();

            }

        }

        return quote;

    }


    @Override
    public CalculatePortfolio calculate(Portfolio portfolio) {

        CalculatePortfolio calculatePortfolio = new CalculatePortfolio();

        for (Stock stock : portfolio.getStocks()){

            Quote quote = null;
            try {
                quote = getQuoteBySymbol(stock.getSymbol());
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            calculatePortfolio.addAllocation(quote, stock);

        }

        return calculatePortfolio;

    }

}