package com.bks.serialize;

import com.bks.model.base.Sector;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

public class SectorSerializer extends JsonSerializer<Sector> {

    @Override
    public void serialize(Sector sector, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {

        serializerProvider.defaultSerializeValue(sector.getName(), jsonGenerator);

    }
}