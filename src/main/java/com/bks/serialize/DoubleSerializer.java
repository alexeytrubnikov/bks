package com.bks.serialize;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

public class DoubleSerializer extends JsonSerializer<Double>{

    @Override
    public void serialize(Double value, JsonGenerator gen, SerializerProvider serializer) throws IOException {

//        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance();
//        formatter.setMinimumFractionDigits(3);
        serializer.defaultSerializeValue(value, gen);

    }
}