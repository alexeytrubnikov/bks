package com.bks.deserialize;

import com.bks.model.base.Company;
import com.bks.model.base.Quote;
import com.bks.model.base.Sector;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;

public class QuoteDeserializer extends JsonDeserializer<Quote> {

    @Override
    public Quote deserialize(JsonParser parser, DeserializationContext ctx) throws IOException, JsonProcessingException {

        ObjectCodec oc = parser.getCodec();
        JsonNode jsonQuote = oc.readTree(parser);

        Quote quote = new Quote();
        quote.setLatestPrice(jsonQuote.get("latestPrice").asDouble());
        quote.setCompany(new Company(jsonQuote.get("symbol").asText(), new Sector(), jsonQuote.get("companyName").asText()));

        return quote;


    }
}