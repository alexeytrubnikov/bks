package com.bks.deserialize;

import com.bks.model.base.Company;
import com.bks.model.base.Sector;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;

public class CompanyDeserializer extends JsonDeserializer<Company> {

    @Override
    public Company deserialize(JsonParser parser, DeserializationContext ctx) throws IOException, JsonProcessingException {

        ObjectCodec oc = parser.getCodec();
        JsonNode jsonCompany = oc.readTree(parser);

        return new Company(jsonCompany.get("symbol").asText(), new Sector(jsonCompany.get("sector").asText()), jsonCompany.get("companyName").asText());

    }
}