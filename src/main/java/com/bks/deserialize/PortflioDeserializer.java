package com.bks.deserialize;

import com.bks.model.inputEntity.Portfolio;
import com.bks.model.inputEntity.Stock;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;

public class PortflioDeserializer extends JsonDeserializer<Portfolio> {

    @Override
    public Portfolio deserialize(JsonParser parser, DeserializationContext ctx) throws IOException, JsonProcessingException {

        ObjectCodec oc = parser.getCodec();
        JsonNode jsonPortfolio = oc.readTree(parser);

        Portfolio portfolio = new Portfolio();

        JsonNode jsonStocks = jsonPortfolio.get("stocks");

        for (JsonNode jsonStock : jsonStocks){
            portfolio.getStocks().add(new Stock(jsonStock.get("symbol").asText(), jsonStock.get("volume").asLong()));
        }

        return portfolio;

    }
}